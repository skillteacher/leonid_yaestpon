using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int coinCost = 1;
    [SerializeField] private LayerMask playerLayer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var layer = 1 << collision.gameObject.layer;
        if ((layer & playerLayer) == 0) return;

        Pickcoin();
    }

    private void Pickcoin()
    {
        Game.game.AddCoins(coinCost);
        Destroy(gameObject);
    }

}
